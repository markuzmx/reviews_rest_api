# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.

RATING_CHOICES = (
    ("1", "1"), ("2", "2"), ("3", "3"), ("4", "4"), ("5", "5"),
    )


class Review(models.Model):
    """Review model"""
    rating = models.CharField(choices=RATING_CHOICES, max_length=1)
    title = models.CharField(max_length=64)
    summary = models.TextField()
    ipaddress = models.CharField(max_length=64)
    date = models.DateTimeField(auto_now_add=True)
    company = models.CharField(max_length=32)
    reviewer_metadata = models.TextField()
    user = models.ForeignKey(User)
