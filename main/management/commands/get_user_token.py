#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# -*- coding: utf-8 -*-
"""

Created on 2019-04-26

@author: Marco Antonio Islas Cruz
"""
from django.contrib.auth.models import User
from django.core.management import BaseCommand
from rest_framework.authtoken.models import Token


class Command(BaseCommand):
    help = "Print the AuthToken for the user"

    def add_arguments(self, parser):
        parser.add_argument("username", type=str, help="username")

    def handle(self, *args, **kwargs):
        try:
            user = User.objects.get(username=kwargs["username"])
        except User.DoesNotExist:
            return
        token, create = Token.objects.get_or_create(user=user)
        token.save()
        print(token.key)
