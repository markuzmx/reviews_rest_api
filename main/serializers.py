#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# -*- coding: utf-8 -*-
"""

Created on 2019-04-26

@author: Marco Antonio Islas Cruz
"""

from django.contrib.auth.models import Group, User
from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault

from main.models import Review


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = ("rating", "title", "summary", "date", "company")

    def create(self, validated_data):
        request = self.context["request"]
        user = request.user
        user_metadata = {
            "name": user.get_full_name(),
            "username": user.username,
            "email": user.email
            }

        ipaddress = request._request.META["REMOTE_ADDR"] or \
                    validated_data.get("ipaddress", "None")
        data = validated_data.copy()
        data["ipaddress"] = ipaddress
        data["user"] = user
        data["reviewer_metadata"] = user_metadata
        obj = Review.objects.create(**data)
        obj.save()
        return obj
