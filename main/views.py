# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals

import json

from rest_framework import status, viewsets, versioning
from rest_framework.authentication import SessionAuthentication, \
    TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

# Create your views here.
from main.models import Review
from main.serializers import ReviewSerializer


class ReviewsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows the views to be viewed or set.
    """
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = ReviewSerializer
    versioning_class = versioning.QueryParameterVersioning

    def get_queryset(self):
        return Review.objects.filter(user=self.request.user).all()
