# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import StringIO
import argparse
import sys

from django.contrib.auth.models import User
from django.core.management import call_command
from django.test import TestCase
# Create your tests here.
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

import main
import reviews_rest_api
from main.apps import MainConfig
from main.management.commands.get_user_token import Command
from main.models import Review


class BasicTest(TestCase):

    def setUp(self):
        """Create basic stuff needed to test"""
        self.user = User.objects.create(
            username="testUser",
            email="test@example.com"
            )
        self.user.set_password("hola1234")
        self.user.save()

        self.user2 = User.objects.create(
            username="testUser2",
            email="test@example.com"
            )
        self.user2.set_password("hola1234")
        self.user2.save()

        self.token = self.get_token(self.user)
        self.token2 = self.get_token(self.user2)

    def get_token(self, user):
        token, create = Token.objects.get_or_create(user=user)
        if create:
            token.save()
        return token

    def tearDown(self):
        for klass in (User, Token, Review):
            [k.delete() for k in klass.objects.all()]


class TestAPI(BasicTest):
    def testLogin(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = client.get('/reviews/')
        self.assertEqual(response.status_code, 200)

    def testWrongLogin(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token asdfasdfasdfasdfasda')
        response = client.get('/reviews/')
        self.assertEqual(response.status_code, 403)

    def testSimpleAdd(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        data = dict(rating="2", title="Title1", summary="summary1",
                    company="exampleCompany", ipaddress="127.0.0.1")
        response = client.post("/reviews/", data, format="json")
        self.assertEqual(response.status_code, 201)
        objs = Review.objects.all()
        self.assertEqual(len(objs), 1)

    def testSimpleAddInvalidData(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        # Basically missing summary, this should return an 400 response
        data = dict(rating="2", title="Title1",
                    company="exampleCompany", ipaddress="127.0.0.1")
        response = client.post("/reviews/", data, format="json")
        self.assertEqual(response.status_code, 400)
        objs = Review.objects.all()
        self.assertEqual(len(objs), 0)

    def testAddTwoTestOneUser(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        data = (dict(rating="2", title="Title1", summary="summary1",
                     company="exampleCompany", ipaddress="127.0.0.1"),
                dict(rating="3", title="Title2", summary="summary2",
                     company="exampleCompany", ipaddress="127.0.0.1")
                )
        for items in data:
            client.post("/reviews/", items, format="json")
        objs = Review.objects.all()
        self.assertEqual(len(objs), 2)
        ndata = []
        for obj in objs:
            el = {
                "rating": obj.rating, "title": obj.title, "summary":
                    obj.summary, "company": obj.company, "ipaddress":
                    obj.ipaddress
                }
            self.assertTrue(el in data)

    def testGetItemsForCurrentUserOnly(self):
        for user in (self.user, self.user2):
            client = APIClient()
            client.credentials(
                HTTP_AUTHORIZATION='Token ' + self.get_token(user).key)
            data = (dict(rating="2", title="Title1", summary="summary1",
                         company="exampleCompany", ipaddress="127.0.0.1"),
                    dict(rating="3", title="Title2", summary="summary2",
                         company="exampleCompany", ipaddress="127.0.0.1")
                    )
            for items in data:
                client.post("/reviews/", items, format="json")
        objs = Review.objects.all()
        # Two users and two items by user, 4 items in total
        self.assertEqual(len(objs), 4)

        # Getting items for one user
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = client.get("/reviews/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)


class CommandsTestCase(BasicTest):
    def test_arguments(self):
        c = Command()
        parser = argparse.ArgumentParser()
        c.add_arguments(parser)
        self.assertEqual(1, 1)

    def test_mycommand(self):
        " Test my custom command."
        args = ["testUser"]
        opts = {}
        # Monkey patching stdout
        out = StringIO.StringIO()
        o_stdout = sys.stdout
        sys.stdout = out
        call_command('get_user_token', *args, **opts)
        out.seek(0)
        response = out.read()
        sys.stdout = o_stdout
        self.assertEqual(response.strip(), self.token.key)

    def test_mycommand_wrong_user(self):
        " Test my custom command."
        args = ["Nobody"]
        opts = {}
        # Monkey patching stdout
        out = StringIO.StringIO()
        o_stdout = sys.stdout
        sys.stdout = out
        call_command('get_user_token', *args, **opts)
        out.seek(0)
        response = out.read()
        sys.stdout = o_stdout
        self.assertEqual(response.strip(), "")

class AppsTestCase(BasicTest):
    def testmain(self):
        c = MainConfig('main', main.apps)
        self.assertEqual(c.name, "main")

