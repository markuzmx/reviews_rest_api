# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from main.models import Review


class AdminReview(admin.ModelAdmin):
    list_display = ("title", "ipaddress", "date", "company", "user")

admin.site.register(Review, AdminReview)
