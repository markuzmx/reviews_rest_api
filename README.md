# reviews_rest_api

This is a simple Django RESTful API that allows a user to send a review and get
reviews done by he/she.

# Setup

## Dependencies

1. Python 2.7
1. VirtualEnv (not required, but recommended)

## Installation

1. Get the code,
1. Install python dependencies.
   * If you want to run this app in a virtualenv, then activate your virtualenv first.
   * `pip install -r requirements.txt`
1. Run the `migrate` command, this will create the database and the required tables.
   * `python manage.py migrate`

## Create users  

1. Create a super user by running this command in the CLI.
   * `python manage.py createsuperuser`
   * It will ask for a username, email and password.
1. Other 'normal' users or other super users can be created in the **admin** interface
   * The admin interface is available in the `/admin/` endpoint.

# Running the App.

1. run `python manage.py runserver`
   * This will create an open connection on `localhost` using the default port `8080`

# Queriying the App.

To query the APP you'll need an application that let you perform HTTP requests as
an example, you can use `curl`

## User Auth with Token
The Application requires the user to authenticate with a Token, for development
poruposes you can get the token using the CLI.

```
python manage.py get_user_token <username>
```

This will return the user token. Please note that the user must exists in the system.

## API Call examples

The only endpoint is the "root", by using the GET method it will return the reviews 
made for the current logged user.

```
 curl http://localhost:8000/reviews/ -H 'Accept: application/json; indent=4' -H 'Content-Type: application/json' -H 'Authorization: Token 37087e246d78c7467c3f2cc6f31b5b92aee0e50f' -X GET
[
    {
        "rating": "3",
        "title": "Titulo1",
        "summary": "asdfasdf",
        "date": "2019-04-26T18:03:50.828316Z",
        "company": "yeyeye"
    },
    {
        "rating": "2",
        "title": "Titulo1",
        "summary": "Python rocks",
        "date": "2019-04-26T18:03:55.034456Z",
        "company": "Example"
    },
    {
        "rating": "5",
        "title": "Titulo1",
        "summary": "summary",
        "date": "2019-04-26T18:14:06.518815Z",
        "company": "TestCompany"
    },
    {
        "rating": "3",
        "title": "Titulo1",
        "summary": "asdasdfasfasdfafasdf",
        "date": "2019-04-26T18:14:48.672650Z",
        "company": "IslasCruz"
    },
]%
```

### POST

In order to add reviews, you can use the "POST" method:

```
curl http://localhost:8000/reviews/ -H 'Accept: application/json; indent=4' -H 'Content-Type: application/json' -H 'Authorization: Token 37087e246d78c7467c3f2cc6f31b5b92aee0e50f' -X POST -d '{"rating" : "3", "title" : "Titulo1", "summary" : "asdfasdf", "ipaddress" : "127.0.0.1", "company" : "Company", "reviewer_metadata":"" }'
```

**As you can see, the Authorization Token is used in every call** 

## Test

To run the tests:

```
$ python manage.py test
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
......
----------------------------------------------------------------------
Ran 6 tests in 0.736s

OK
Destroying test database for alias 'default'...
```

This perform a set of tests to prove the code works fine.

## Coverage Tests

You can use coverage to check how much of the code is being tested.

First run coverate to run the django tests:

```
coverage run --source='.main' manage.py test
```

Then ask coverage to create the report:

```
Name                                      Stmts   Miss  Cover
-------------------------------------------------------------
main/__init__                                 0      0   100%
main/admin                                    6      0   100%
main/apps                                     4      0   100%
main/management/__init__                      1      0   100%
main/management/commands/__init__             1      0   100%
main/management/commands/get_user_token      16      0   100%
main/migrations/0001_initial                  8      0   100%
main/migrations/0002_auto_20190426_1732       5      0   100%
main/migrations/__init__                      0      0   100%
main/models                                  13      0   100%
main/serializers                             21      0   100%
main/tests                                  117      0   100%
main/views                                   16      0   100%
-------------------------------------------------------------
TOTAL                                       208      0   100%
```



